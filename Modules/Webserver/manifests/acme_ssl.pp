# Import the ACME module
include acme

# Define a certificate resource for example.com
acme::certificate { 'xyz.local':
  ensure        => 'present',
  challenge     => 'http',
  www_root      => '/var/www',
  privatekey    => '/etc/ssl/private/xyz.key',
  fullchain     => '/etc/ssl/certs/xyz.crt',
  notify_service => 'apache2',
  domains       => ['example.com', 'www.example.com', 'subdomain.example.com'],
}

# Schedule certificate renewal (e.g., daily at midnight)
cron { 'acme_renew_cert':
  command => '/usr/bin/puppet agent -t', # Run the Puppet agent to trigger renewal
  hour    => '0',
  minute  => '0',
}
