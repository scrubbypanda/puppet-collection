# manifests/init.pp
$server_name = 'example.com'
$document_root = '/var/www/html'
$ssl_cert_file = '/etc/ssl/certs/example.crt'
$ssl_key_file = '/etc/ssl/private/example.key'

file { '/etc/apache2/sites-available/example.conf':
  content => epp('module_name/virtualhost.epp'),
}

service { 'apache2':
  ensure => 'running',
  enable => true,
}

